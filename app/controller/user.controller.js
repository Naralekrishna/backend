const db = require("../config/db.config.js");
const config = require("../config/config.js");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { table } = require("../config/db.config.js");
var moment = require("moment-timezone");

const User = db.users;
const JWTToken = db.jwttokon;
const Employee = db.employee;
const Attendance = db.attendance;
const Table = db.table;
const AllocateTable = db.allocatetable;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

exports.signup = (req, res) => {
  if (!req.body.Email) {
    return res
      .status(400)
      .send({ message: "Your request Email is missing details." });
  } else {
    if (!req.body.Password) {
      return res
        .status(400)
        .send({ message: "Your request Password is missing details." });
    } else {
      // if (!req.body.UserName) {
      // 	return res.status(400).send({ message:"Your request UserName is missing details."});
      // }
      // else {
      const EmailToValidate = req.body.Email;
      const passToValidate = req.body.Password;
      const EmailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
      const mailcheck = EmailRegexp.test(EmailToValidate);
      const Passwordcheck = /^.{2,12}$/;
      const passcheck = Passwordcheck.test(passToValidate);
      if (!mailcheck) {
        //console.log(EmailToValidate);
        res.status(500).json({
          message: "Please Enter A Correct Email like abc@example.com ",
        });
      } else {
        if (!passcheck) {
          res.status(500).json({
            message: "Password length should be Minimum 2 and Maximum 12",
          });
          //	console.log("Password is not match ");
        } else {
          bcrypt.hash(req.body.Password, 10, function (err, hash) {
            if (err) {
              return res.status(500).json({
                error: err,
              });
            } else {
              User.findOne({ where: { Email: req.body.Email } }).then(function (
                user
              ) {
                if (!user) {
                  console.log("new user");
                  const user = new User({
                    Email: req.body.Email,
                    //UserName: req.body.UserName,
                    Password: hash,
                  });

                  user
                    .save()
                    .then(function (result) {
                      const JWTToken1 = jwt.sign(
                        { Email: user.Email, id: user.id },
                        config.secret,
                        {
                          //	expiresIn: 86400 // expires in 24 hours
                        }
                      );

                      console.log("req 2... ");

                      const uid = user.id;
                      const jwttokon = new JWTToken({
                        UserId: uid,
                        Status: true,
                        Token: JWTToken1,
                      });
                      jwttokon.save().then(function (result) {
                        res.status(200).json({
                          id: uid,
                          success: "200",
                          message: "New user has been created",
                          token: JWTToken1,
                        });
                      });
                    })
                    .catch((error) => {
                      res.status(500).json({
                        error: err,
                      });
                    });
                } else {
                  console.log("user is already present");
                  res.status(500).json({
                    success: "404",
                    message: "User Already Present in database",
                  });
                }
              });
            }
          });
        }
      }
      // }
    }
  }
};

exports.signin = (req, res) => {
  if (!req.body.Email) {
    return res
      .status(401)
      .json({ message: " Your request email is missing details. " });
  } else {
    if (!req.body.Password) {
      return res
        .status(401)
        .json({ message: "Your request username is missing details." });
    }
  }

  User.findOne({ where: { Email: req.body.Email } }).then((user) => {
    if (!user) {
      //console.log(user)

      return res.status(401).json({
        success: "404",
        message: "Unauthorized Access,User Account Is Deactivated",
      });
    } else {
      //	console.log(user)

      User.findOne({
        where: { Email: req.body.Email /*, Password: req.body.Password,*/ },
      })
        .then((user) => {
          if (!user) {
            return res.status(401).json({
              success: "404",
              message: "Unauthorized Access,Enter Correct Email And Password",
            });
          }
          var passwordIsValid = bcrypt.compareSync(
            req.body.Password,
            user.Password
          );
          if (!passwordIsValid) {
            return res.status(401).json({
              success: "404",
              message: "Unauthorized Access,Enter Correct Email And Password",
            });
          }
          const userid = user.userid;
          const jwt = require("jsonwebtoken");
          const JWTToken1 = jwt.sign(
            { Email: user.Email, id: user.id },
            config.secret,
            {}
          );
          const uid = user.id;
          console.log(uid);
          console.log("save");
          const jwttokon = new JWTToken({
            UserId: uid,
            Status: true,
            Token: JWTToken1,
          });

          jwttokon.save().then(function (result) {
            res.status(200).json({
              success: "200",
              message: "Welcome To The Application",
              id: uid,
              token: JWTToken1,
            });
          });
        })
        .catch((err) => {
          res.status(500).send("Error -> " + err);
        });
    }
  });
};

exports.logout = (req, res) => {
  const UserId = req.body.id;
  let token = req.header("x-access-token");

  JWTToken.update(
    { Status: "0" },
    { where: { UserId: UserId, Token: token } }
  ).then((jwttokon) => {
    res.status(200).json({
      success: "200",
      message: "logout Successfully..",
      UserId: UserId,
    });
  });
};

exports.getAllEmp = (req, res) => {
  Employee.findAll().then((emp) => {
    res.status(200).json({
      success: "200",
      message: "All Employee",
      info: emp,
    });
  });
};

exports.clockIn = (req, res) => {
  // var DateNow = new Date();
  var moment = require("moment-timezone");
  var Dt = moment().tz("Asia/Kolkata").format();

  if (!req.body.Emp_id) {
    return res
      .status(401)
      .json({ message: " Your request Employee Id is missing details. " });
  }
  if (!req.body.Password) {
    return res
      .status(401)
      .json({ message: "Your request Password is missing details." });
  }

  Employee.findOne({ where: { Emp_id: req.body.Emp_id } }).then((user) => {
    if (req.body.Password == user.Password) {
      Attendance.create({
        Restorent_id: req.body.Restorent_id,
        Emp_id: req.body.Emp_id,
        Date: Dt,
        // Date:DateNow.getUTCDate(),
        LoginTime: Dt,
        IsLoggedIn: "1",
      }).then((clockin) => {
        return res.status(401).json({
          success: "200",
          message: "ClockIn Successfully",
          Data: clockin,
        });
      });
    } else {
      return res.status(401).json({
        success: "404",
        message: "Wrong Password",
      });
    }
  });
};

exports.clockOut = (req, res) => {
  var Dt = moment().tz("Asia/Kolkata").format();

  if (!req.body.Emp_id) {
    return res
      .status(401)
      .json({ message: " Your request Employee Id is missing details. " });
  }
  if (!req.body.Password) {
    return res
      .status(401)
      .json({ message: "Your request Password is missing details." });
  }

  Employee.findOne({ where: { Emp_id: req.body.Emp_id } }).then((user) => {
    if (req.body.Password == user.Password) {
      Attendance.update(
        { IsLoggedIn: "0", LogoutTime: Dt },
        { where: { Emp_id: user.Emp_id } }
      ).then((clockout) => {
        var CL = { status: "0" };
        return res.status(200).json({
          success: "200",
          message: "ClockOut Successfully",
          Data: CL,
        });
      });
    } else {
      return res.status(401).json({
        success: "404",
        message: "Wrong Password",
      });
    }
  });
};

exports.ActiveEmployee = (req, res) => {
  Attendance.findAll({
    where: { Restorent_id: req.body.Restorent_id, IsLoggedIn: "1" },
  }).then((emp) => {
    res.status(200).json({
      success: "200",
      message: "Active Employees",
      info: emp,
    });
  });
};

exports.LoginForOrder = (req, res) => {
  if (!req.body.Emp_id) {
    return res
      .status(401)
      .json({ message: " Your request Employee Id is missing details. " });
  }
  if (!req.body.Password) {
    return res
      .status(401)
      .json({ message: "Your request Password is missing details." });
  }

  Employee.findOne({ where: { Emp_id: req.body.Emp_id } }).then((user) => {
    if (req.body.Password == user.Password) {
      return res.status(200).json({
        success: "200",
        message: "Login Successfully",
      });
      // })
    } else {
      return res.status(401).json({
        success: "404",
        message: "Wrong Password",
      });
    }
  });
};

exports.AllRestorentTables = (req, res) => {
  Table.findAll({ where: { Restorent_id: req.body.Restorent_id } }).then(
    (table) => {
      res.status(200).json({
        success: "200",
        message: "All Tables",
        Tables: table,
      });
    }
  );
};

exports.AllocatedTable = async function (req, res) {
  AllocateTable.findAll({
    where: { Restorent_id: req.body.Restorent_id, Emp_id: req.body.Emp_id },
  }).then(async (alctable) => {
    // console.log(alctable.length)

    // var act= JSON.parse(JSON.stringify(alctable))
    // var UserArr=[]
    // 	for (var i = 0; i < act.length; i++) {

    // 		  console.log(act[i].TableNumber)
    // 		var data=[]
    // 		Table.findOne({where:{id:act[i].TableNumber}}).then(table => {

    // 				// data.push(table)
    // 				var actlll= JSON.parse(JSON.stringify(table))
    // 				console.log(actlll)

    // 				res.status(200).json({

    // 					success:'200',
    // 					message:'All Tables',
    // 					Tables:actlll
    // 		});
    // 		})

    // 		// UserArr[i] = data
    // 		// console.log(data)
    // 	}
    // 		// var tres= JSON.parse(JSON.stringify(table))
    // 		// 	 Data.push(tres)
    // 		// 	console.log(Data)
    // 		// 	res.status(200).json({

    // 		// 			success:'200',
    // 		// 			message:'All Tables',
    // 		// 			Tables:Data
    // 		// 		});

    // 		//})

    // //  }

    var UserArr = [];

    Table.findAll().then((table) => {
      for (var i = 0; i < alctable.length; i++) {
        var data = [];
        for (var j = 0; j < table.length; j++) {
          if (alctable[i].TableNumber == table[j].id) {
            data.push(table[j]);
            //console.log(userimags)
          }
        }
        UserArr[i] = data;
        var Udata = (UserArr.status = 0);
        // console.log(data)
      }
      res.status(200).json({
        success: "200",
        message: "All Tables",
        Tables: Udata,
      });
    });
  });
};

exports.TableAllo = (req, res) => {
  var Restorent_id = req.body.Restorent_id;
  var Emp_id = req.body.Emp_id;

  db.sequelize
    .query(
      "SELECT tables.id, tables.Restorent_id ,tables.TableNumber FROM tables " +
        " inner join allocatetables on tables.id= allocatetables.TableNumber "
    )
    .spread(function (results, metadata) {
      console.log(results);

      res.status(200).json({
        success: "200",
        message: "Display Successfully...",
        result: results,
      });
    });
};
