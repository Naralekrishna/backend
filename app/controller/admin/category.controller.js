const db = require("./../../config/db.config");

const Category = db.category;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

exports.index = async (req, res) => {
  Category.findAll()
    .then((categories) => {
      res.status(200).json({
        success: "200",
        message: "All Category",
        data: { categories },
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.store = (req, res) => {
  !req.body.Name
    ? res.status(400).send({ message: "Your request Name is missing details." })
    : Category.create(req.body)
        .then((category) => {
          res.status(201).json({
            success: "201",
            message: "Category created successfully",
            data: { category },
          });
        })
        .catch((err) => {
          console.log(err);
        });
};
