const db = require("./../../config/db.config");

const SubCategory = db.subCategory;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;

exports.index = async (req, res) => {
  SubCategory.findAll()
    .then((subCategories) => {
      res.status(200).json({
        success: "200",
        message: "All SubCategory",
        data: { subCategories },
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.store = (req, res) => {
  if (!req.body.CategoryId)
    return res
      .status(400)
      .send({ message: "Your request CategoryId is missing details." });
  if (!req.body.Name)
    return res
      .status(400)
      .send({ message: "Your request Name is missing details." });
  if (!req.body.Price)
    return res
      .status(400)
      .send({ message: "Your request Price is missing details." });

  SubCategory.create({
    CategoryId: req.body.CategoryId,
    Name: req.body.Name,
    Price: req.body.Price,
    Status: true,
  })
    .then((subCategory) => {
      res.status(201).json({
        success: "201",
        message: "SubCategory created successfully",
        data: { subCategory },
      });
    })
    .catch((err) => {
      console.log(err);
    });
};
