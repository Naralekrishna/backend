const sequelize = require("sequelize");
module.exports = (sequelize, Sequelize) => {
  const SubCategory = sequelize.define("subCategory", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    CategoryId: {
      type: Sequelize.INTEGER,
    },
    Name: {
      type: Sequelize.STRING,
    },
    Price: {
      type: Sequelize.INTEGER,
    },
    Status: {
      type: Sequelize.BOOLEAN,
    },
  });
  return SubCategory;
};
