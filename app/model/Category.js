const sequelize = require("sequelize");
module.exports = (sequelize, Sequelize) => {
  const Category = sequelize.define("category", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    Name: {
      type: Sequelize.STRING,
    },
  });
  return Category;
};
