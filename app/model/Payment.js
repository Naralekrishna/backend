const sequelize = require("sequelize");
module.exports = (sequelize, Sequelize) => {
  const Payment = sequelize.define("payment", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    OrderId: {
      type: Sequelize.INTEGER,
    },
    TotalAmount: {
      type: Sequelize.INTEGER,
    },
    Discount: {
      type: Sequelize.INTEGER,
    },
    TaxAmount: {
      type: Sequelize.INTEGER,
    },
  });
  return Payment;
};
