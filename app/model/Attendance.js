const sequelize = require('sequelize');
module.exports = (sequelize, Sequelize) => {
    const Attendance = sequelize.define('attendance', {
        id: {
            type: Sequelize.INTEGER,
            
            autoIncrement: true,
            primaryKey: true
        },
        Restorent_id: {
            type: Sequelize.INTEGER,
        },
        Emp_id: {
            type: Sequelize.INTEGER,
        },
        Emp_Name: {
            type: Sequelize.STRING,
        },
        Date: {
            type: Sequelize.DATE,
        },
        LoginTime: {
            type: Sequelize.STRING,
        },
        LogoutTime: {
            type: Sequelize.STRING,
        },
        Active: {
            type: Sequelize.BOOLEAN,
        },
        IsLoggedIn: {
            type: Sequelize.BOOLEAN,
        },
        
    });  
    return Attendance;
}
