const sequelize = require('sequelize');
module.exports = (sequelize, Sequelize) => {
    const Employee = sequelize.define('employee', {
        id: {
            type: Sequelize.INTEGER,
            
            autoIncrement: true,
            primaryKey: true
        },
        Restorent_id: {
            type: Sequelize.INTEGER,
        },
        Name: {
            type: Sequelize.STRING,
        },
        Emp_id: {
            type: Sequelize.STRING,
        },
        Password: {
            type: Sequelize.STRING,
        },
        
    });  
    return Employee;
}
