module.exports = (sequelize, Sequelize) => {
	const Jwttoken = sequelize.define('jwttoken', {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        Token: {
            type: Sequelize.STRING
        },
        Status: {
            type: Sequelize.BOOLEAN
        },
        
	});
	return Jwttoken;
}