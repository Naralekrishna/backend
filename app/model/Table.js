const sequelize = require("sequelize");
module.exports = (sequelize, Sequelize) => {
  const Table = sequelize.define("table", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    Restorent_id: {
      type: Sequelize.INTEGER,
    },
    TableNumber: {
      type: Sequelize.STRING,
    },
  });
  return Table;
};
