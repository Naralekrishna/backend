const sequelize = require("sequelize");
module.exports = (sequelize, Sequelize) => {
  const Order = sequelize.define("order", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    RestaurantId: {
      type: Sequelize.INTEGER,
    },
    EmployeeId: {
      type: Sequelize.INTEGER,
    },
    TableId: {
      type: Sequelize.INTEGER,
    },
    Status: {
      type: Sequelize.BOOLEAN,
    },
  });
  return Order;
};
