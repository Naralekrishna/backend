const sequelize = require("sequelize");
module.exports = (sequelize, Sequelize) => {
  const OrderedItems = sequelize.define("orderedItems", {
    id: {
      type: Sequelize.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    OrderId: {
      type: Sequelize.INTEGER,
    },
    Item: {
      type: Sequelize.INTEGER,
    },
    Price: {
      type: Sequelize.INTEGER,
    },
    Quantity: {
      type: Sequelize.INTEGER,
    },
  });
  return OrderedItems;
};
