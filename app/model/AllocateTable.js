const sequelize = require('sequelize');
module.exports = (sequelize, Sequelize) => {
    const AllocateTable = sequelize.define('Allocatetable', {
        id: {
            type: Sequelize.INTEGER,
            
            autoIncrement: true,
            primaryKey: true
        },
        Restorent_id: {
            type: Sequelize.INTEGER,
        },
        Emp_id: {
            type: Sequelize.INTEGER,
        },
        Emp_Name: {
            type: Sequelize.STRING,
        },
        TableNumber: {
            type: Sequelize.STRING,
        },
        Active: {
            type: Sequelize.BOOLEAN,
        },
       
        
    });  
    return AllocateTable;
}
