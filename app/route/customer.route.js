module.exports = function (app) {

    const authJwt = require('./verifyJwtToken');
    const users = require('../controller/user.controller.js');

  
    app.post('/api/signup', users.signup); 
    app.post('/api/signin', users.signin); 
    app.put('/api/logout',users.logout);
    app.get('/api/getAllEmp', users.getAllEmp); 
    app.post('/api/clockIn', users.clockIn); 
    app.post('/api/clockOut', users.clockOut); 
    app.get('/api/ActiveEmployee', users.ActiveEmployee); 
    app.post('/api/LoginForOrder', users.LoginForOrder); 
    app.get('/api/AllRestorentTables', users.AllRestorentTables); 
    app.get('/api/AllocatedTable', users.AllocatedTable); 


    app.get('/api/TableAllo', users.TableAllo); 


}  