const category = require("./../../controller/admin/category.controller");

module.exports = function (app) {
  //   Routes
  app.get("/api/categories", category.index);
  app.post("/api/categories", category.store);
};
