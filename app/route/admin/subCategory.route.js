const subCategory = require("./../../controller/admin/subCategory.controller");

module.exports = function (app) {
  //   Routes
  app.get("/api/subCategories", subCategory.index);
  app.post("/api/subCategories", subCategory.store);
};
