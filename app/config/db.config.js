const Sequelize = require("sequelize");
const env = require("./env.js");
const sequelize = new Sequelize(env.database, env.username, env.password, {
  host: env.host,
  port: env.port,
  dialect: env.dialect,
  operatorsAliases: 0,
  logging: false,

  pool: {
    max: env.max,
    min: env.pool.min,
    acquire: env.pool.acquire,
    idle: env.pool.idle,
  },
});
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("../model/Restuser.js")(sequelize, Sequelize);
db.jwttokon = require("../model/jwttoken.js")(sequelize, Sequelize);
db.employee = require("../model/employee.js")(sequelize, Sequelize);
db.attendance = require("../model/Attendance.js")(sequelize, Sequelize);
db.allocatetable = require("../model/AllocateTable.js")(sequelize, Sequelize);
db.table = require("../model/Table.js")(sequelize, Sequelize);
db.category = require("../model/Category.js")(sequelize, Sequelize);
db.subCategory = require("../model/SubCategory.js")(sequelize, Sequelize);
db.order = require("../model/Order.js")(sequelize, Sequelize);
db.orderedItems = require("../model/OrderedItems.js")(sequelize, Sequelize);
db.payment = require("../model/Payment.js")(sequelize, Sequelize);

// Here we can connect users and jwttoken base on users'id
db.users.hasMany(db.jwttokon, { foreignKey: "id" });
db.jwttokon.belongsTo(db.users, { foreignKey: "UserId" });

module.exports = db;
